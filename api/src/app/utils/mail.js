const nodemailer = require("nodemailer");

module.exports = (function () {
  const transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT || 587,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS,
    },
  });

  const defaultOptions = {
    from: `${process.env.MAIL_NAME} <${process.env.MAIL_USER}>`,
    to: null,
    subject: '',
    text: undefined, // plain text body
    html: undefined, // html body
  }
  
  function send(options) {
    return transporter
    .sendMail({
      ...defaultOptions,
      ...options,
    });
  }

  return {
    send
  };
})();
