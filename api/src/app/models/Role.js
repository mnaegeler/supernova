module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define("Role", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    hasFullAccess: DataTypes.BOOLEAN,
    labelKey: DataTypes.STRING,
    level: DataTypes.INTEGER,
    reference: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Role.associate = (models) => {
    Role.hasMany(models.ModelFieldPermission, {
      as: "ModelFieldPermissions",
      constraints: false,
    });
    Role.hasMany(models.ModelPermission, {
      as: "ModelPermissions",
      constraints: false,
    });
    Role.hasMany(models.User, { as: "Users", constraints: false });
    Role.hasMany(models.ViewPermission, {
      as: "ViewPermissions",
      constraints: false,
    });
    Role.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return Role;
};
