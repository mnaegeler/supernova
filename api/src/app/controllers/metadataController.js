const fs = require("fs").promises;
const util = require("util");
const path = require("path");
const { Op } = require("sequelize");

const models = require("../models");
const Auth = require("../facades/Auth");
const cache = require("../../config/cache");

async function getViewMetadata(role = null) {
  if (!role) {
    role = {
      id: null,
      hasFullAccess: false,
    };
  }

  const cacheKey = `metadata_${role.id}_${role.hasFullAccess}`;
  const hasCached = cache.get(cacheKey);
  if (hasCached) {
    return JSON.parse(hasCached);
  }

  const retrieve = await models["View"].findAll({
    attributes: [
      "id",
      "key",
      "labelKey",
      "type",
      "slug",
      "showOnMenu",
      "filters",
      "include",
    ],
    include: [
      {
        model: models["ViewPermission"],
        as: "ViewPermissions",
        required: !role.hasFullAccess,
        attributes: ["id", "RoleId"],
        where: {
          RoleId: {
            [Op.eq]: role.id,
          },
        },
      },

      {
        model: models["Model"],
      },

      {
        model: models["MenuSection"],
        attributes: ["id", "labelKey"],
      },

      {
        model: models["ViewField"],
        as: "ViewFields",
        attributes: {
          exclude: ["createdAt", "updatedAt", "ModelFieldId", "ViewId"],
        },
        order: [["order", "ASC"]],
        include: [
          {
            model: models["ModelField"],
            required: true,
            attributes: {
              exclude: ["createdAt", "updatedAt", "ModelFieldId", "ViewId"],
            },
            include: [
              {
                model: models["ModelFieldPermission"],
                as: "ModelFieldPermissions",
                required: !role.hasFullAccess,
                attributes: ["id"],
                where: {
                  RoleId: role.id,
                },
              },
              "TargetModelField",
            ],
          },
        ],
      },

      {
        model: models["ViewAction"],
        as: "ViewActions",
        attributes: [
          "id",
          "type",
          "labelKey",
          "class",
          "slug",
          "params",
          "order",
          "position",
        ],
        order: [["order", "ASC"]],
      },

      {
        model: models["ViewRowAction"],
        as: "ViewRowActions",
        attributes: [
          "id",
          "type",
          "labelKey",
          "class",
          "slug",
          "params",
          "order",
          "position",
        ],
        order: [["order", "ASC"]],
      },
    ],
  });
  
  cache.set(cacheKey, JSON.stringify(retrieve), 900);
  return retrieve;
}

const metadataController = {
  async getMeta(req, res) {
    const user = Auth.user();
    let languageAbbr = "en";

    if (user && user.Language && user.Language.active) {
      languageAbbr = user.Language.abbr;
    } else if (req.get("Language")) {
      languageAbbr = req.get("Language");
    }

    let labels = null;
    let error = "";
    try {
      const jsonPath = path.resolve(
        __dirname,
        "../..",
        "labels",
        `${languageAbbr}.json`
      );
      labels = await fs.readFile(jsonPath, "utf8");
    } catch (e) {
      error = `Translation file for language ${languageAbbr} not found`;
    }

    if (!error) {
      try {
        labels = JSON.parse(labels);
      } catch (e) {
        error = `Translation file for language ${languageAbbr} has syntax errors`;
      }
    }

    if (error) {
      console.error(error);
      return res.status(500).send({ messages: [error] });
    }

    try {
      let views = await getViewMetadata(user ? user.Role : null);
      let organizations = [];
      if (user) {
        organizations = await models["OrganizationUser"].findAll({
          where: { UserId: user.id },
          include: models["Organization"],
        });
      }

      const fileStoragePath =
        process.env.FILE_STORAGE_PATH || "/public/uploads";

      return res.status(200).send({
        views,
        labels,
        user,
        organizations,
        isSessionEmulated: Auth.isSessionEmulated(),
        fileStoragePath,
      });
    } catch (e) {
      console.error(e);
      return res.status(500).send(e);
    }
  },
};

module.exports = metadataController;
