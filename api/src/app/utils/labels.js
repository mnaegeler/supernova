const path = require("path");
const fs = require("fs").promises;

async function getLabel(
  language,
  labelKey,
  defaultValue = "",
  replacements = null
) {
  const jsonPath = path.resolve(
    __dirname,
    "../..",
    "labels",
    `${language}.json`
  );
  let labels = await fs.readFile(jsonPath, "utf8");
  labels = JSON.parse(labels);

  let resultLabel = labels[labelKey] || defaultValue || labelKey;
  if (replacements) {
    for (let key in replacements) {
      resultLabel = resultLabel.replace(
        new RegExp(`#${key}`, "g"),
        replacements[key]
      );
    }
  }

  return resultLabel;
}

module.exports = { getLabel };
