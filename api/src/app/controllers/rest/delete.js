const Permission = require("../../facades/Permission");
const { preActionValidation, logError } = require('./_commonActions');

module.exports = async function deleteRecord(user, modelName, data) {
  const models = require("../../models");

  await preActionValidation(modelName, user, "delete");

  let element = null;
  const id = typeof data === 'string' ? data : data.id;

  try {
    element = await models[modelName].findByPk(id);
  } catch (e) {
    throw new Error("error/record-not-found");
  }

  try {
    await element.destroy();
  } catch (e) {
    logError(e);
    throw new Error(e);
  }

  return id;
};