const fs = require("fs").promises;
const path = require("path");
const prettier = require("prettier");

// Paths
const modelsPath = path.resolve(__dirname, "..", "app", "models");
const metadataPath = path.resolve(__dirname, "..", "app", "metadata");
const modelTemplatePath = path.resolve(
  __dirname,
  "..",
  "templates",
  "model.js.tpl"
);

const modelsNotToUpdate = [
  // !important! User should not be updated because of hooks and custom functions...
  "User",
  // There is a case with ChildViewId that should be resolved to work properly
  "ViewField",
  // There is a case with defaultValue that should be resolved to work properly
  "Language",
];

async function readAndParseFile(path) {
  const file = await fs.readFile(path, "utf-8");
  try {
    return JSON.parse(file);
  } catch (e) {
    console.error(path);
    throw e;
  }
}

function order(a, b) {
  if (a < b) {
    return -1;
  }

  if (a > b) {
    return 1;
  }

  return 0;
}

async function init() {
  console.time("models");
  const metaFiles = await fs.readdir(metadataPath);
  const modelsToUpdate = metaFiles.filter(
    (modelName) => modelsNotToUpdate.includes(modelName) === false
  );

  const modelTemplateFile = await fs.readFile(modelTemplatePath, "utf-8");

  for (let modelIndex = 0; modelIndex < modelsToUpdate.length; modelIndex++) {
    const metaFile = modelsToUpdate[modelIndex];
    let modelData;
    const fields = [];
    const relations = [];

    try {
      modelData = await readAndParseFile(
        path.resolve(metadataPath, metaFile, "Model.json")
      );
    } catch (error) {
      console.error(error);
      return;
    }

    if (modelData.deletedAt) {
      continue;
    }

    const fieldFiles = await fs.readdir(
      path.resolve(metadataPath, metaFile, "ModelFields")
    );

    for (let fieldIndex = 0; fieldIndex < fieldFiles.length; fieldIndex++) {
      const field = await readAndParseFile(
        path.resolve(
          metadataPath,
          metaFile,
          "ModelFields",
          fieldFiles[fieldIndex]
        )
      );

      if (field.deletedAt) {
        continue;
      }

      if (
        !["createdAt", "updatedAt", "deletedAt", "CreatedByUserId"].includes(
          field.name
        )
      ) {
        if (["belongsTo", "hasMany", "file"].includes(field.type)) {
          relations.push(
            `{#name}.${field.type}(models.${field.targetModel}${
              field.targetModelAlias &&
              field.targetModelAlias !== field.targetModel
                ? `, { as: "${field.targetModelAlias}", constraints: false }`
                : ", { constraints: false }"
            });`
          );
        } else {
          fields.push(`${field.name}: ${field.modelFieldType},`);
        }
      }
    }
    
    let parsed = modelTemplateFile;
    parsed = parsed.replace(/\{#fields\}/g, fields.sort(order).join(""));
    parsed = parsed.replace(
      /\{#relations\}/g,
      relations.sort(order).join("")
    );
    parsed = parsed.replace(/\{#name\}/g, modelData.name);

    const pretty = prettier.format(parsed, { parser: "babel" });
    await fs.writeFile(
      path.resolve(modelsPath, `${modelData.name}.js`),
      pretty
    );
  }

  console.timeEnd("models");
}

process.argv.forEach((val, index) => {
  if (
    val === "-s" &&
    process.argv[index - 1].includes("src/generators/models.js")
  ) {
    init();
  }
});

module.exports = { writeModelsToFiles: init };
