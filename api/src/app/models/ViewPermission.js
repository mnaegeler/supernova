module.exports = (sequelize, DataTypes) => {
  const ViewPermission = sequelize.define("ViewPermission", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },

    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewPermission.associate = (models) => {
    ViewPermission.belongsTo(models.Role, { constraints: false });
    ViewPermission.belongsTo(models.View, { constraints: false });
    ViewPermission.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return ViewPermission;
};
