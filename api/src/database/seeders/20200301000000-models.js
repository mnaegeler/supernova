'use strict';
const { init } = require('../../generators/import')

module.exports = {
  up: async () => {
    await init(false);
  },

  down: () => {
    return new Promise(res => res())
    // queryInterface.bulkDelete('ViewFields', null, {truncate: true}),
    // queryInterface.bulkDelete('ModelFields', null, {truncate: true}),
    // queryInterface.bulkDelete('Views', null, {truncate: true}),
    // queryInterface.bulkDelete('Models', null, {truncate: true}),
  }
};
