const path = require("path");

// const models = require("../models");
const { exportModelMetadata } = require("../../generators/export");
const { DataTypes } = require("sequelize");
const { writeModelsToFiles } = require("../../generators/models");
const { synchronizeDatabase } = require("../../generators/sync-database");
const cache = require("../../config/cache");

function camelToSnake(str) {
  return str.replace(
    /[A-Z]/g,
    (letter, index) => `${index > 0 ? "_" : ""}${letter.toLowerCase()}`
  );
}

async function createListView(modelInstance) {
  const slugName = camelToSnake(modelInstance.name);
  const { View, ViewAction, ViewRowAction, MenuSection } = require("../models");
  const primaryMenu = await MenuSection.findOne({ where: { labelKey: 'PrimaryMenuSection' } });

  const listView = await View.create({
    ModelId: modelInstance.id,
    MenuSectionId: primaryMenu.id,
    key: `list-${slugName}`,
    labelKey: modelInstance.name,
    type: "ListView",
    slug: `/${slugName}`,
    showOnMenu: true,
    filters: null,
    include: null,
  });

  await ViewAction.create({
    ViewId: listView.id,
    labelKey: "create",
    class: null,
    type: "navigation",
    slug: `/${slugName}_form`,
    order: 1,
    position: "top right",
  });

  await ViewRowAction.bulkCreate([
    {
      ViewId: listView.id,
      labelKey: "edit",
      class: "is-link",
      type: "navigation",
      slug: `/${slugName}_form/:id:`,
      order: 1,
    },
    {
      ViewId: listView.id,
      labelKey: "remove",
      class: "has-text-danger",
      type: "delete",
      order: 2,
      params: { id: ":id:" },
    },
  ]);
}

async function createFormView(modelInstance) {
  const slugName = camelToSnake(modelInstance.name);
  const { View } = require("../models");

  await View.create({
    ModelId: modelInstance.id,
    key: `form-${slugName}`,
    labelKey: modelInstance.name,
    type: "FormView",
    slug: `/${slugName}_form`,
    showOnMenu: false,
    filters: null,
    include: null,
  });

  /*
  await ViewAction.bulkCreate([
    {
      ViewId: formView.id,
      labelKey: "back",
      class: null,
      type: "navigation",
      slug: `/${slugName}`,
      order: 1,
      position: "top left",
    },
    {
      ViewId: formView.id,
      labelKey: "save",
      class: "is-info",
      type: "submit",
      slug: `/${slugName}`,
      order: 1,
      position: "form end",
    },
  ]);
  */
}

async function createShowView(modelInstance) {
  const slugName = camelToSnake(modelInstance.name);
  const { View, ViewAction } = require("../models");

  const showView = await View.create({
    ModelId: modelInstance.id,
    key: `show-${slugName}`,
    labelKey: modelInstance.name,
    type: "ShowView",
    slug: `/${slugName}_view`,
    showOnMenu: false,
    filters: null,
    include: null,
  });

  await ViewAction.create({
    ViewId: showView.id,
    labelKey: "edit",
    class: null,
    type: "navigation",
    slug: `/${slugName}_form`,
    order: 1,
    position: "top right",
  });

  await ViewAction.create({
    ViewId: showView.id,
    labelKey: "remove",
    class: null,
    type: "delete",
    slug: null,
    order: 2,
    position: "top right",
    params: { id: ":id:" },
  });
}

module.exports = {
  async afterCreate(instances) {
    for (let instance of instances.new) {
      await createListView(instance);
      await createFormView(instance);
      await createShowView(instance);

      const modelName = instance.name;

      await exportModelMetadata(modelName);
      await writeModelsToFiles();
      await synchronizeDatabase(modelName);
    }

    cache.emit('clearMetadata');
  },

  async afterUpdate(instances) {
    for (let instance of instances.new) {
      const modelName = instance.name;
      await exportModelMetadata(modelName);
      await writeModelsToFiles();
      await synchronizeDatabase(modelName);
    }

    cache.emit('clearMetadata');
  },
};
