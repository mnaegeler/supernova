import { writable } from 'svelte/store';

let user = writable(null);
let isSessionEmulated = writable(null);
let authToken = writable(null);
let views = writable(null);
let labels = writable(null);
let currentOrganization = writable(null);
let organizations = writable(null);
let message = writable(null);
let notifications = writable([]);
let fileStoragePath = writable(null);

let deleteRecordModal = writable({
  id: null,
  isActive: false,
  modelName: null,
  callback: null,
});

export {
  user,
  isSessionEmulated,
  authToken,
  views,
  labels,
  currentOrganization,
  organizations,
  message,
  notifications,
  fileStoragePath,
  deleteRecordModal,
};
