const jwt = require("jsonwebtoken");
const { Op } = require("sequelize");
const { promisify } = require("util");
const Auth = require("../facades/Auth");
const { UserSessionEmulate } = require("../models");

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization || req.query.authorization || "";
  const isMetadataOrLogin =
    req.originalUrl === "/api/metadata" ||
    req.originalUrl === "/api/extract-metadata" ||
    req.originalUrl === "/api/sessions" ||
    req.originalUrl.startsWith("/public");

  if (!authHeader && !isMetadataOrLogin) {
    return res.status(401).json({ message: "Token not provided" });
  }

  const [, token] = authHeader.split(" ");

  try {
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET);

    let userId = decoded.id;
    let sessionEmulated = null;
    // , startDate: { [Op.lte]: new Date() }, endDate: { [Op.gte]: new Date() }
    const sessionEmulate = await UserSessionEmulate.findOne({
      where: { CreatedByUserId: decoded.id, TargetUserId: { [Op.not]: null } },
    });
    if (sessionEmulate && req.headers.model !== "UserSessionEmulate") {
      userId = sessionEmulate.TargetUserId;
      sessionEmulated = sessionEmulate.id;
    }

    await Auth.loadUser(userId, sessionEmulated);

    return next();
  } catch (err) {
    console.log("deu erro");
    console.log(err);
    if (isMetadataOrLogin) {
      return next();
    }

    return res.status(401).json({ message: "Invalid token" });
  }
};
